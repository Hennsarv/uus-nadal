﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KonsooliAken
{
    public partial class TeineAken : Form
    {
        public string Nimi = "";

        public TeineAken()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uus = new TeineAken();
            uus.Nimi = this.textBox1.Text;
            uus.Show();
        }

        private void TeineAken_Load(object sender, EventArgs e)
        {
            this.Text = this.Nimi;
        }
    }
}
