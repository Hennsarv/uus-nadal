﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DanmebaasiKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\tooted.txt";
            using (SqlConnection conn =
                new SqlConnection("Data Source=hennusql.database.windows.net;Initial Catalog=northwind22;Persist Security Info=True;User ID=student;Password=Pa$$w0rd"))
            {
                using (SqlCommand comm = new SqlCommand("select * from products", conn))
                {
                    conn.Open();

                    var R = comm.ExecuteReader();

                    List<string> read = new List<string>();
                    while (R.Read())
                    {
                        Console.WriteLine($"toode {R["ProductName"]} maksab {R["UnitPrice"]}");
                        read.Add($"{R["ProductName"]},{R["UnitPrice"]}");
                    }
                    System.IO.File.WriteAllLines(filename, read);

                }
            }

        }
    }
}
