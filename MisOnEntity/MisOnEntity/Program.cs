﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEntity
{
    partial class Employee
    {
        public string Fullname => $"{FirstName} {LastName}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            northwind22Entities ne = new northwind22Entities();

            ne.Database.Log = Console.WriteLine;

            foreach(var x in ne.Products.Where(p => p.UnitPrice < 10))
                Console.WriteLine($"{x.ProductName} {x.UnitPrice}");

            foreach(var x in ne.Employees)
                Console.WriteLine($"{x.Fullname} ülemus on {x.Manager?.Fullname ?? "tema ise"}");

            var segane = from x in ne.Products
                         where x.UnitPrice < 10
                         orderby x.UnitPrice descending
                         select new { x.ProductID, x.ProductName, x.UnitPrice };

            var selgem = ne.Products
                            .Where(x => x.UnitPrice < 10)
                            .OrderByDescending(x => x.UnitPrice)
                            .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice });

            segane.ToList().ForEach(x => Console.WriteLine(x));

            Console.WriteLine(
            ne.Categories.Where(x => x.CategoryID == 8).Single().CategoryName
            );
            ne.Categories.Where(x => x.CategoryID == 8).Single().CategoryName = "Seatoit";
            Console.WriteLine(
            ne.Categories.Where(x => x.CategoryID == 8).Single().CategoryName
            );
            ne.SaveChanges();
        }
    }
}
